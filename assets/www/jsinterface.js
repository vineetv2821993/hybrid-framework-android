function callCamera(imgName,Directory){
	//Save Image Source of give imgName to Given Directory
	window.JSInterface.callCamera(imgName,Directory);
}

function callAccel(coordinate){
	//Where coordinate could be 'x','y','z','X','Y','Z'. Otherwise it return 0.
	return window.JSInterface.callAccel(coordinate);
}

function callCompass(){
	//return orientation based on current ORIENTATION_SENSOR
	return window.JSInterface.callCompass();
}

function returnLatitudeVal(){
	return window.JSInterface.callGPSLatitude();
}

function returnLongitudeVal(x,y){
	return window.JSInterface.callGPSLongitude();
}

function returnCurrentLocation(){
	return window.JSInterface.CoordinateToLocation(window.JSInterface.callGPSLatitude(),window.JSInterface.callGPSLongitude());

}

function returnLocation(x,y){
	return window.JSInterface.CoordinateToLocation(x,y);
}