package com.example.unisyshybridappframework;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Random;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;


@SuppressLint("JavascriptInterface")
public class Interface extends Activity{
	
	
	public Interface(){
	}
	
	private static WebView webview;
	protected void settingInterface(String Location){	
		webview = new WebView(this);
		setContentView(webview);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.addJavascriptInterface(new JSInterface(), "JSInterface");
		webview.loadUrl(Location);	
	}
	
	public static WebView returnWebView(){
		return webview;
	}
	public Context returnContext(){
		return this;
	}
	//-----------For Camera Calling... Changes Should Now Can Done In Camera Class-----------------//
	public void cameraIntent(String ImageName, String Directory){
		
		Camera.myCallCamera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		Camera.ImageSource = ImageName;
		Camera.Directory = Directory;
		startActivityForResult(Camera.myCallCamera,Camera.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);	
		
	}
	
	
	
	//-----------Start of javaScript interface-----------------//
	public class JSInterface{
		
		public JSInterface(){


		    }

		

	   //-----------Calling The Camera-----------------//
		public void  callCamera(String Imagename, String Directory){//Calling Camera
				
				MainActivity.returnWebView().setBackgroundColor(Color.GREEN);
				cameraIntent(Imagename,Directory);
			}	
		//-----------String handling-----------------//
		public String strReturn(String str){//Setting Desire String
			return str;
		}
		
		//-----------String Accelerometer-----------------//
		public double  callAccel(String str){//Calling Accel
			if(str.length()==1){
			 SensorManager temp = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
			 MainActivity.returnWebView().setBackgroundColor(Color.GREEN);
			 return (new Accelerometer().onCreate(temp,str.charAt(0)));
			}
			return 0.0;

		}	
		//GPS Functionality
		public double  callGPSLatitude(){//Calling GPS
			MainActivity.returnWebView().setBackgroundColor(Color.GREEN);
			return GPS.getGeoByLocation(returnContext(),0);
		}	
		public double  callGPSLongitude(){//Calling GPS
			MainActivity.returnWebView().setBackgroundColor(Color.GREEN);
			return GPS.getGeoByLocation(returnContext(),1);
		}	
		public String CoordinateToLocation(double x, double y){
			return new GPS().ConvertPointToLocation(x, y, getBaseContext());
		}

		/////Compass//////
		public double callCompass(){//Calling Compass
			Compass.onCreate(returnContext());
			return Compass.azimuth;
		}
	}	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == Camera.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
	        Camera.thumbnail = (Bitmap) data.getExtras().get("data");  
	        File myDir=new File(Camera.Directory);
	        myDir.mkdirs();
	        Random generator = new Random();
	        int n = 10000;
	        n = generator.nextInt(n);
	        String fname = Camera.ImageSource;
	        File file = new File (myDir, fname);
	        if (file.exists ()) file.delete (); 
	        try {
	               FileOutputStream out = new FileOutputStream(file);
	               Camera.thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, out);
	               out.flush();
	               out.close();

	        } catch (Exception e) {
	               e.printStackTrace();
	        }
	    }

	} 
	

	  @Override
	  protected void onDestroy() {
	    super.onDestroy();
	    if (Compass.sensor != null) {
	      Compass.sensorService.unregisterListener(Compass.mySensorEventListener);
	    }
	  }


}