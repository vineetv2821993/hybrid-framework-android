package com.example.unisyshybridappframework;



import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;



public class Compass {

	  public static SensorManager sensorService;
	  public static Sensor sensor;
	  public static float azimuth;
	  public MainActivity myact;

	  
	/** Called when the activity is first created. */


	  public static void onCreate(Context context) {
		    sensorService = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		    sensor = sensorService.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		    if (sensor != null) {
		      sensorService.registerListener(mySensorEventListener, sensor,
		          SensorManager.SENSOR_DELAY_NORMAL);
		    } 

	  }

	  public static SensorEventListener mySensorEventListener = new SensorEventListener() {

	    @Override
	    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	    }

	    @Override
	    public void onSensorChanged(SensorEvent event) {
	      // angle between the magnetic north directio
	      // 0=North, 90=East, 180=South, 270=West
	      azimuth = event.values[0];

	    }
	  };
	  
}
