package com.example.unisyshybridappframework;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Accelerometer implements SensorEventListener{
	
	private static float x, y, z;

	private SensorManager mSensorManager;

	private Sensor mAccelerometer;


    public double onCreate(SensorManager Service, char var) {

        mSensorManager = Service;

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        switch(var){
        	case 'x':
        	case 'X': return x;
        	case 'y':
        	case 'Y': return y;
        	case 'z':
        	case 'Z': return z;
        	default : return 0.0;	
        }

    }


    	@Override

    	public void onAccuracyChanged(Sensor sensor, int accuracy) {

    	// can be safely ignored for this demo

    	}
    	
    	@Override

    	public void onSensorChanged(SensorEvent event) {


    		x = event.values[0];

    		y = event.values[1];

    		z = event.values[2];

    	}

    
}