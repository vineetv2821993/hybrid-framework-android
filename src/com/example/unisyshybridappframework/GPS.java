package com.example.unisyshybridappframework;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;

public class GPS {
	
    public static double getGeoByLocation(Context context, int index) {
    	
    	LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); 
    	Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double geoLatitude = 0.0;
        double geoLongitude = 0.0;
        try {
            if (location != null) {
                geoLatitude = location.getLatitude() * 1E6;
                geoLongitude = location.getLongitude() * 1E6;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       if(index == 0) return geoLatitude;
       if(index == 1) return geoLongitude;
       else return 0.0;
    }
	
   public String ConvertPointToLocation(double x, double y, Context context) {   
        String address = "";
        Geocoder geoCoder = new Geocoder(
                context, Locale.getDefault());//getBaseContext
        try {
            List<Address> addresses = geoCoder.getFromLocation(
               x  / 1E6, 
                y / 1E6, 1);

            if (addresses.size() > 0) {
                for (int index = 0; index < addresses.get(0).getMaxAddressLineIndex(); index++)
                    address += addresses.get(0).getAddressLine(index) + " ";
            }
        }
        catch (IOException e) {                
            e.printStackTrace();
        }   

        return address;
    } 
   

}
