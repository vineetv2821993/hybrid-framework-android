package com.example.unisyshybridappframework;

import android.content.Intent;
import android.graphics.Bitmap;

public class Camera {
	public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	public static Intent myCallCamera;
	public static Bitmap thumbnail;
	public static String ImageSource, Directory;

}
